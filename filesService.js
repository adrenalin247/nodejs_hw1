const {
  getFilename,
  getFilesList,
  getLocalFile,
  getContent,
  if_file_error_send_400,
  if_filename_error_send_400,
} = require('./routerUtils');

const fs = require('fs');
const path = require('path');

const dir = path.dirname('./files/*');

function getFiles(req, res, next) {
  const files = fs.readdirSync(dir);

  res.status(200).send({
    message: 'Success',
    files: files,
  });
}

const getFile = (req, res, next) => {
  const filename = getFilename(req);
  const fileError = if_file_error_send_400(filename, dir, true, res);

  if (!fileError) {
    const file = getLocalFile(filename, dir);
    const extension = path.extname(file).slice(1);
    const content = fs.readFileSync(file).toString();
    const uploadedDate = fs.statSync(file).birthtime;

    res.status(200).send({
      message: 'Success',
      filename,
      content,
      extension,
      uploadedDate,
    });
  }
};

function createFile(req, res, next) {
  const filename = getFilename(req);
  const filenameError = if_filename_error_send_400(filename, res);

  if (!filenameError) {
    const file = getLocalFile(filename, dir);
    const content = getContent(req);

    fs.writeFileSync(file, content);
    res.status(200).send({ message: 'File created successfully' });
  }
}

function editFile(req, res, next) {
  const filename = getFilename(req);
  const content = getContent(req);

  const fileError = if_file_error_send_400(filename, dir, true, res);
  if (!fileError) {
    const file = getLocalFile(filename, dir);
    const stream = fs.createWriteStream(file, { flags: 'a' });
    stream.write(content);
    stream.end();
    res.status(200).send({ message: 'File edited successfully' });
  }
}

function deleteFile(req, res, next) {
  const filename = getFilename(req);
  console.log(req.params);

  const fileError = if_file_error_send_400(filename, dir, true, res);
  if (!fileError) {
    const file = getLocalFile(filename, dir);
    fs.unlinkSync(file);
    res.status(200).send({ message: 'File deleted successfully' });
  }
}

function deleteFiles(req, res, next) {
  const fileList = getFilesList(req);
  console.log(fileList);

  const fileErrorMessage = fileList.find((el) =>
    if_file_error_send_400(el, dir, true, res)
  );

  if (!fileErrorMessage) {
    fileList.forEach((el) => {
      const file = getLocalFile(el, dir);
      fs.unlinkSync(file);
    });
    res.status(200).send({ message: 'Files deleted successfully' });
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
  deleteFiles,
};
