const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const app = express();

const { filesRouter } = require('./filesRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

//ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  let message = 'Server error';
  if (err) message = err;

  console.error(message);
  res.status(500).send({ message: message });
}
