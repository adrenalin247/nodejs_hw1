const express = require('express');
const router = express.Router();
const {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
  deleteFiles,
} = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:filename', getFile);

router.put('/', editFile);

router.delete('/:filename', deleteFile);

router.delete('/', deleteFiles);

module.exports = {
  filesRouter: router,
};
