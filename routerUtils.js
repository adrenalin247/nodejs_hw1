const path = require('path');
const fs = require('fs');

function getContent(req) {
  return req.body.content;
}

function getFilename(req) {
  const filename = req.body.filename || req.path.slice(1);
  return filename || '';
}

function getFilesList(req) {
  return req.body.files || [];
}

function getFilenameErrorMessage(filename) {
  if (!filename instanceof String && !filename.length > 0) {
    return `Filename cannot be empty`;
  }

  const fileExtension = path.extname(filename).slice(1);
  const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

  if (!allowedExtensions.includes(fileExtension)) {
    return `File extension '${fileExtension}' is not allowed`;
  }

  return '';
}

function if_filename_error_send_400(filename, res) {
  const filenameError = getFilenameErrorMessage(filename);
  if (filenameError) {
    res.status(400).send({ message: filenameError });
    return filenameError;
  }
  return null;
}

function getLocalFile(filename, dir) {
  if (filename && dir) {
    return path.resolve(`${dir}/${filename}`);
  }
  return null;
}

function getFileErrorMessage(file, isExist = true) {
  if (isExist) {
    try {
      fs.lstatSync(file).isFile();
      return '';
    } catch (e) {
      const filename = path.basename(file);
      return `No file with '${filename}' filename found`;
    }
  }
}

function if_file_error_send_400(filename, dir, isExist, res) {
  const filenameError = if_filename_error_send_400(filename, res);
  if (filenameError) return filenameError;

  const file = getLocalFile(filename, dir);

  const fileErrorMessage = getFileErrorMessage(file, isExist);
  if (!fileErrorMessage) return null;

  res.status(400).send({ message: fileErrorMessage });
  return fileErrorMessage;
}

module.exports = {
  getFilename,
  getFilesList,
  getLocalFile,
  getContent,
  if_file_error_send_400,
  if_filename_error_send_400,
};
